//*****************************************************
//David Masangcay
//CS481
//Homework 3
//9/24/2020
//*****************************************************
import 'dart:ffi';

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

void main() {runApp(MyApp());}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Widget titleSection = Container(
      padding: const EdgeInsets.all(32),
      child: Row(
        children: [
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container
                  (
                  padding: const EdgeInsets.only(bottom: 8),
                  child: Text('Peanut Butter Jelly Sandwhich',
                      style: TextStyle(fontWeight: FontWeight.bold,)),
                ),
                Icon(Icons.star, color: Colors.yellow[500]),
                Text('4.97/5', style: TextStyle(color: Colors.black,))
              ],
            ),
          ),
        ],
      ),
    );

    Widget textSection = Container(
      padding: const EdgeInsets.all(32),
      child: Text(
        'The amazing Peanut Butter Jelly Sandwich recipe by the one and only David Masangcay'
            'is one of the greatest recipes ever created.  This recipe is used by almost all American families to'
            ' ensure their kids enjoy a delicious meal at a low price!',
        softWrap: true,),
    );

    return MaterialApp(
      title: 'Homework 2',
      home: Scaffold(
        appBar: AppBar(
          title: Text('Famous Recipes'),
        ),
        body: ListView(
            children: [
              Image.asset(
                'images/pbj.jpg', width: 650, height: 240, fit: BoxFit.cover,),
              titleSection,
              FavoriteWidget(),
              textSection,
            ]
        ),
      ),
    );
  }
}

class FavoriteWidget extends StatefulWidget {
  @override
  _FavoriteWidgetState createState() => _FavoriteWidgetState();
}

class _FavoriteWidgetState extends State<FavoriteWidget> {
  bool b1bool = true, b2bool = true, b3bool = true, b4bool = true;
  int b1count = 0, b2count = 0, b3count = 62, b4count = 27;
  @override

  //Each button Widget builds
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [

        Container(
          margin: const EdgeInsets.only(top: 8),
          child: IconButton(
            icon: (b1bool ? Icon(Icons.forward) : Icon(Icons.favorite_border)),
            color: Colors.blue[500],
            onPressed: toggle1Favorite,
          ),
        ),
        SizedBox(
          width: 18,
          child: Container(
            child:Text('$b1count'),
          ),
        ),

        Container(
          margin: const EdgeInsets.only(top: 8),
          child: IconButton(
            icon: (b2bool ? Icon(Icons.access_time) : Icon(Icons.favorite_border)),
            color: Colors.blue[500],
            onPressed: toggle2Favorite,
          ),
        ),
        SizedBox(
          width: 18,
          child: Container(
            child:Text('$b2count'),
          ),
        ),

        Container(
          margin: const EdgeInsets.only(top: 8),
          child: IconButton(
            icon: (b3bool ? Icon(Icons.thumb_up) : Icon(Icons.favorite_border)),
            color: Colors.blue[500],
            onPressed: toggle3Favorite,
          ),
        ),
        SizedBox(
          width: 18,
          child: Container(
            child:Text('$b3count'),
          ),
        ),

        Container(
          margin: const EdgeInsets.only(top: 8),
          child: IconButton(
            icon: (b4bool ? Icon(Icons.rate_review) : Icon(Icons.favorite_border)),
            color: Colors.blue[500],
            onPressed: toggle4Favorite,
          ),
        ),
        SizedBox(
          width: 18,
          child: Container(
            child:Text('$b4count'),
          ),
        ),
      ],
    );
  }

  //Functions for each button
  void toggle1Favorite() {
    setState(() {
      if(b1bool) {
        b1count += 1;
        b1bool = true;
      }

      else {
        b1count -= 1;
        b1bool = false;
      }
    });
  }

  void toggle2Favorite() {
    setState(() {
      if(b2bool) {
        b2count += 1;
        b2bool = true;
      }

      else {
        b2count -= 1;
        b2bool = false;
      }
    });
  }

  void toggle3Favorite() {
    setState(() {
      if(b3bool) {
        b3count += 1;
        b3bool = true;
      }

      else {
        b3count -= 1;
        b3bool = false;
      }
    });
  }

  void toggle4Favorite() {
    setState(() {
      if(b4bool) {
        b4count += 1;
        b4bool = true;
      }

      else {
        b4count -= 1;
        b4bool = false;
      }
    });
  }
}